from django.db import models

# Create your models here.
class Profile(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    # TODO: replace this hack with subquery
    def balance(self):
        return sum([x.diff for x in self.transactions.all()])
    def transactions(self):
        return self.transactions.all()

class Transaction(models.Model):
    profile = models.ForeignKey(Profile, related_name='transactions', on_delete=models.CASCADE)
    diff = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)