from django.db.models import Sum
import graphene
from graphene_django import DjangoObjectType
from graphene_django.forms.mutation import DjangoFormMutation


from tx.forms import ProfileForm, TransactionForm
from tx.models import Profile, Transaction

class TransactionType(DjangoObjectType):
    class Meta:
        model = Transaction
        fields = ("id", "diff", "created_at", "profile")

class ProfileType(DjangoObjectType):
    balance = graphene.Int()
    transactions = graphene.List(TransactionType)
    class Meta:
        model = Profile
        fields = "__all__"
    def resolve_balance(self, info):
        return self.balance()

    def resolve_transactions(self, info):
        return Transaction.objects.filter(profile=self).all()



class ProfileMutation(DjangoFormMutation):
    class Meta:
        form_class = ProfileForm

class TransactionMutation(DjangoFormMutation):
    class Meta:
        form_class = TransactionForm

class Query(graphene.ObjectType):
    all_transactions = graphene.List(TransactionType)
    all_profiles = graphene.List(ProfileType)
    profile_by_name = graphene.Field(ProfileType, name=graphene.String(required=True))
    profile_by_email = graphene.Field(ProfileType, email=graphene.String(required=True))

    def resolve_all_transactions(root, info):
        return Transaction.objects.select_related("profile").all()

    def resolve_all_profiles(root, info):
        return Profile.objects.all()

    def resolve_profile_by_name(root, info, name):
        try:
            return Profile.objects.get(name=name)
        except Profile.DoesNotExist:
            return None

    def resolve_profile_by_email(root, info, email):
        try:
            return Profile.objects.get(email=email)
        except Profile.DoesNotExist:
            return None

class Mutation(graphene.ObjectType):
    update_transaction = TransactionMutation.Field()
    update_profile = ProfileMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)