import json

from graphene_django.utils.testing import GraphQLTestCase

class TxCase(GraphQLTestCase):
    GRAPHQL_URL = "/graphql"
    def test_add_some_profile_query(self):
        response = self.query(
            '''
            mutation {
              updateProfile(input:{name: "name", email: "fff@ttt.gg"}) {
                name
                email
              }
            }

            '''
        )
        print(response.content)
        content = json.loads(response.content)
        self.assertResponseNoErrors(response)
    def test_some_query(self):
        response = self.query(
            '''
             {
                allProfiles {
                    id
                    name
                    balance
                }
            }
            '''
        )
        content = json.loads(response.content)
        self.assertResponseNoErrors(response)