from django.forms import ModelForm
from tx.models import Profile, Transaction

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['name', 'email']

class TransactionForm(ModelForm):
    class Meta:
        model = Transaction
        fields = ['diff', 'profile']