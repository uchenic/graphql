from django.contrib import admin
from tx.models import Profile, Transaction

admin.site.register(Profile)
admin.site.register(Transaction)

# Register your models here.
